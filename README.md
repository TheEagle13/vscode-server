# Code-Server Container Build

Dockerfile config that will create a container image based on Amazon Linux 2 that will Install Code-Server (VS Code in the cloud) and Chromium accessible via noVNC to access locally built servers. Also includes additional software and local settings as required for development. 


## Software Installed:
Latest Node  
Hashicorp Packer and Terraform  
Git  
Python3  
Yarn  
Chromium  
Java-11-amazon-corretto-headless (Utilized by sonarlint to avoid using Oracle Java)  
TigerVNC Server  
noVNC  
VIM  
Nano  
Ruby  
Additional Libraries and Dependencies as required

## Code-Server Extensions Installed:
aeschli.vscode-css-formatter  
michelemelluso.code-beautifier  
ms-vscode.node-debug2  
sonarsource.sonarlint-vscode  
auchenberg.vscode-browser-preview  
hashicorp.terraform  
wholroyd.hcl  
hookyqr.beautify  
ms-python.python  

## To Run The Container after Building
`docker run -it -d -p 5001:8080 -p 6001:4000 -e username="<Git User Name>" -e useremail="<Git User Email>" -v /home/ec2-user/node_modules:/node_modules --name <container name> --privileged --restart always <registry location/project name>/vscode-server`   
doc
(Replace the port 5001 and 6001 with the ports you wish to attach to on the docker host, Additionally replace the the sections in <> with the applicable info)
