FROM amazonlinux

### Copy required files/Scripts ###
RUN mkdir /configs
COPY configs/* /configs/
COPY installscripts/* /tmp/

### Run Global Setup ###
RUN /bin/bash /tmp/globalinstall.sh

### Run User Setup ###
RUN sudo -u coder -H /bin/bash /tmp/userinstall.sh

### Cleanup ###
RUN yum -y clean all; rm -rf /var/tmp/* /var/cache/yum/* /tmp/*

USER 1000
EXPOSE 8080
EXPOSE 4000
ENV USER=coder
LABEL version=2.0
WORKDIR /work
ENTRYPOINT /configs/entrypoint.sh

