#!/bin/bash
### Install node ###
echo "**********Installing Node**********"
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
. ~/.nvm/nvm.sh
nvm install node
node_ver=$(node -v)
echo '
    "sonarlint.pathToNodeExecutable": "/home/coder/.nvm/versions/node/'$node_ver'/bin/node"
}' >> /home/coder/.local/share/code-server/settings.json
cp /home/coder/.local/share/code-server/settings.json /home/coder/.local/share/code-server/User/settings.json

### Install Code-Server Extensions ###
echo "**********Installing Code-Server**********"
export SERVICE_URL=https://marketplace.visualstudio.com/_apis/public/gallery
export ITEM_URL=https://marketplace.visualstudio.com/items
EXTENSIONS=('aeschli.vscode-css-formatter' 'michelemelluso.code-beautifier' 'ms-vscode.node-debug2' 'sonarsource.sonarlint-vscode' 'auchenberg.vscode-browser-preview' 'hashicorp.terraform' 'wholroyd.hcl' 'hookyqr.beautify' 'ms-python.python')
for pkg in "${EXTENSIONS[@]}"
do 
    code-server --install-extension "$pkg"
done 

### Activate/Initialize Conda ###
echo "**********Conda Init Starting**********"
/opt/conda/bin/activate
/opt/conda/bin/conda init

### Configure global git ignore ###
echo "#Ignore node_modules directories
node_modules/" >> /home/coder/.gitignore_global
