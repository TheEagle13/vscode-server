#! /bin/bash
### Update and install dependencies ###
echo "**********Installing Dependencies**********"
cd /tmp
yum update -y
amazon-linux-extras install -y epel
curl --silent --location https://dl.yarnpkg.com/rpm/yarn.repo | tee /etc/yum.repos.d/yarn.repo
yum install -y git python3 yarn chromium java-11-amazon-corretto-headless libXcomposite libXcursor libXi libXtst libXrandr alsa-lib alsa-firmware alsa-tools-firmware mesa-libEGL libXdamage mesa-libGL libXScrnSaver chromium tigervnc-server-minimal novnc vim nano sudo shadow-utils wget passwd tar net-tools ruby yum-utils
yum-config-manager --add-repo https://rpm.releases.hashicorp.com/AmazonLinux/hashicorp.repo
yum install -y packer terraform

### Create non root user ###
echo "**********Creating Coder User**********"
adduser coder && echo "coder ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers.d/nopasswd
passwd -d coder

### Create/Provision Working Directories ###
echo "**********Provisioning Working Directories**********"
mkdir -p /work /node_modules /home/coder/.local/share/code-server/User/
rm -rf /usr/share/novnc/index.html
mv /configs/index.html /usr/share/novnc/index.html
mv /configs/settings.json /home/coder/.local/share/code-server/
mv /configs/coder.json /home/coder/.local/share/code-server/
chown -R coder:coder /work /node_modules /home/coder
chmod 755 /configs/*

### Install Conda ###
echo "**********Installing Conda**********"
wget https://repo.anaconda.com/archive/Anaconda3-2020.07-Linux-x86_64.sh -O /tmp/conda.sh
bash /tmp/conda.sh -b -p /opt/conda

### Install Code-Server ###
echo "**********Installing Code-Server***********"
version=$(curl -s https://github.com/coder/code-server/releases/latest | awk -F '[/"]' '{print $9}' | awk '{print substr($1,2); }')
curl -fL "https://github.com/coder/code-server/releases/download/v${version}/code-server-${version}-amd64.rpm" -o "/tmp/code-server-${version}-amd64.rpm"
rpm -i /tmp/code-server-${version}-amd64.rpm
