#!/bin/bash
set -e
### Set Global Git Config ###
git config --global user.name "$username"
git config --global user.email "$useremail"
git config --global core.excludesfile ~/.gitignore_global

### Start up Chromium via VNC and code-server ###
/usr/bin/Xvnc :0 -nolisten tcp -UseIPv4 -interface lo -localhost -SecurityTypes None -rfbport 5900 -desktop chrome -nevershared -noclipboard -noreset -DisconnectClients -screen scrn 1280x1024x24 &
/usr/bin/chromium-browser --display=:0 --disable-gpu --disable-software-rasterizer --disable-dev-shm-usage --disable-plugins --disable-plugins-discovery --disable-notifications --disable-sync --mute-audio --dns-prefetch-disable --noremote --window-size=1280,1024 --no-first-run &
/usr/bin/novnc_server --listen 4000 --web /usr/share/novnc --vnc localhost:5900 &
/usr/bin/code-server --disable-telemetry --auth none --host 0.0.0.0 --port 8080
